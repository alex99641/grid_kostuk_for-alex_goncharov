﻿
namespace Lab5
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.RandomNumberTextBox = new System.Windows.Forms.TextBox();
            this.FirstMethodButton = new System.Windows.Forms.Button();
            this.SecondMethodButton = new System.Windows.Forms.Button();
            this.ThirdMethodButton = new System.Windows.Forms.Button();
            this.StartThreadingButton = new System.Windows.Forms.Button();
            this.StopThreadsButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(13, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(280, 315);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Location = new System.Drawing.Point(319, 13);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(280, 315);
            this.panel2.TabIndex = 1;
            // 
            // RandomNumberTextBox
            // 
            this.RandomNumberTextBox.Location = new System.Drawing.Point(623, 13);
            this.RandomNumberTextBox.Multiline = true;
            this.RandomNumberTextBox.Name = "RandomNumberTextBox";
            this.RandomNumberTextBox.Size = new System.Drawing.Size(315, 315);
            this.RandomNumberTextBox.TabIndex = 2;
            // 
            // FirstMethodButton
            // 
            this.FirstMethodButton.Location = new System.Drawing.Point(34, 347);
            this.FirstMethodButton.Name = "FirstMethodButton";
            this.FirstMethodButton.Size = new System.Drawing.Size(224, 23);
            this.FirstMethodButton.TabIndex = 3;
            this.FirstMethodButton.Text = "Отрисовка прямоугольников";
            this.FirstMethodButton.UseVisualStyleBackColor = true;
            this.FirstMethodButton.Click += new System.EventHandler(this.FirstMethodButton_Click);
            // 
            // SecondMethodButton
            // 
            this.SecondMethodButton.Location = new System.Drawing.Point(345, 347);
            this.SecondMethodButton.Name = "SecondMethodButton";
            this.SecondMethodButton.Size = new System.Drawing.Size(224, 23);
            this.SecondMethodButton.TabIndex = 4;
            this.SecondMethodButton.Text = "Отрисовка эллипсов";
            this.SecondMethodButton.UseVisualStyleBackColor = true;
            this.SecondMethodButton.Click += new System.EventHandler(this.SecondMethodButton_Click);
            // 
            // ThirdMethodButton
            // 
            this.ThirdMethodButton.Location = new System.Drawing.Point(668, 347);
            this.ThirdMethodButton.Name = "ThirdMethodButton";
            this.ThirdMethodButton.Size = new System.Drawing.Size(224, 23);
            this.ThirdMethodButton.TabIndex = 5;
            this.ThirdMethodButton.Text = "Сгенерировать числа";
            this.ThirdMethodButton.UseVisualStyleBackColor = true;
            this.ThirdMethodButton.Click += new System.EventHandler(this.ThirdMethodButton_Click);
            // 
            // StartThreadingButton
            // 
            this.StartThreadingButton.Location = new System.Drawing.Point(232, 423);
            this.StartThreadingButton.Name = "StartThreadingButton";
            this.StartThreadingButton.Size = new System.Drawing.Size(224, 43);
            this.StartThreadingButton.TabIndex = 6;
            this.StartThreadingButton.Text = "Запустить все методы";
            this.StartThreadingButton.UseVisualStyleBackColor = true;
            this.StartThreadingButton.Click += new System.EventHandler(this.StartThreadingButton_Click);
            // 
            // StopThreadsButton
            // 
            this.StopThreadsButton.Location = new System.Drawing.Point(503, 423);
            this.StopThreadsButton.Name = "StopThreadsButton";
            this.StopThreadsButton.Size = new System.Drawing.Size(224, 43);
            this.StopThreadsButton.TabIndex = 7;
            this.StopThreadsButton.Text = "Остановить выполнение потоков";
            this.StopThreadsButton.UseVisualStyleBackColor = true;
            this.StopThreadsButton.Click += new System.EventHandler(this.StopThreadsButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(958, 504);
            this.Controls.Add(this.StopThreadsButton);
            this.Controls.Add(this.StartThreadingButton);
            this.Controls.Add(this.ThirdMethodButton);
            this.Controls.Add(this.SecondMethodButton);
            this.Controls.Add(this.FirstMethodButton);
            this.Controls.Add(this.RandomNumberTextBox);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox RandomNumberTextBox;
        private System.Windows.Forms.Button FirstMethodButton;
        private System.Windows.Forms.Button SecondMethodButton;
        private System.Windows.Forms.Button ThirdMethodButton;
        private System.Windows.Forms.Button StartThreadingButton;
        private System.Windows.Forms.Button StopThreadsButton;
    }
}

